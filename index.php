        <?php
            # read the file into an array, else, output error
            $artistFile = file("data-files/artists.txt") or die("ERROR! Unable to open artists file!!!");
            $paintingsFile = file("data-files/paintings.txt") or die("ERROR! Unable to open paintings file!!!");

            #our file is tilde delimited
            $delimiter = "~";
            
            $i = 0;
            foreach ($artistFile as $artist){
                $artistFields = explode($delimiter, $artist);
                
                $artistFile[$i] = [
                                    "artist_id"=>$artistFields[0],
                                    "fname" => $artistFields[1], 
                                    "lname" => $artistFields[2], 
                                    "nationality" => $artistFields[3], 
                                    "yob" => $artistFields[4], 
                                    "yod" => $artistFields[5], 
                                    "artistDesc" => $artistFields[6], 
                                    "artistURL" => $artistFields[7]
                                    ];
            $i++;
            }
            
            $j = 0;
            foreach ($paintingsFile as $paintings){
                $paintingsFields = explode($delimiter, $paintings);
                $paintingsFile[$j] = [
                                    "rndmNo1"=>$paintingsFields[0],
                                    "rndmNo2" => $paintingsFields[1], 
                                    "rndmNo3" => $paintingsFields[2], 
                                    "paintingID" => $paintingsFields[3], 
                                    "paintingTitle" => $paintingsFields[4], 
                                    "paintingDesc" => $paintingsFields[5], 
                                    "paintingYear" => $paintingsFields[6], 
                                    "width" => $paintingsFields[7],
                                    "height" => $paintingsFields[8],
                                    "genre" => $paintingsFields[9],
                                    "gallery" => $paintingsFields[10],
                                    "price" => $paintingsFields[11],
                                    "imgURL" => $paintingsFields[12]
                                    ];
            $j++;
            }
        ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Lab3 SE3316A</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- FONTS -->
        <link href='https://fonts.googleapis.com/css?family=Parisienne' rel='stylesheet' type='text/css'>
        
        
        <!-- LINKS & SCRIPTS -->
        <link rel="stylesheet" href="styles.css">
        
            <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        
            <!-- jQuery library -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        
            <!-- Latest compiled JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
        
         <!-- NAV BAR -->
        <div class="container">
        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-static-top">
                  <div class="container-fluid">
                    
                    <div class="navbar-header">
                        
                      <a class="navbar-brand" href="#"><h1>Lab 3</h1></a>
                      
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="work.php">Work</a></li>
                        <li><a href="artist.php">Artists</a></li>
                      </ul>
                    </div>
                  </div>
                </nav>
            </div>
        </div>
        </div>
            
        <!-- Carousel-->
        <!-- data ride says start carousel immediately, class= carousel says this is a carousel, and class=slide indicates transition-->
        <div id="carousel" class="carousel slide" data-ride="carousel">
        
            
            <!-- Indicators: datatarget=carousel id, data-slide-to=which slide to go to when you click -->
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
                <li data-target="#carousel" data-slide-to="3"></li>
                <li data-target="#carousel" data-slide-to="4"></li>
            </ol>
        
            <!-- Wrapper for slides -->
            <!-- class=item=content of slide -->
            <div class="carousel-inner">
        <?php
                for ($m=0 ; $m<5 ; $m++){
                    
                if($m == 0)
                {
                 echo "<div class='item active'>";
                }
                else {
                 echo "<div class='item'>";
                }
        ?>          
                    <img class="caroimg" src='art-images/paintings/large/<?php echo $paintingsFile[$m]["paintingID"]; ?>.jpg' alt ="<?php echo $paintingsFile[$m]["paintingTitle"]; ?>" >
                    <div class='carousel-caption'>
                        <h1 class="shad"><?php echo $paintingsFile[$m]['paintingTitle']; ?></h1>
                        <h3 ><?php echo $paintingsFile[$m]['paintingYear']; ?></h3>
                        <p>
                            <br>
                            <a class='btn btn-primary btn-lg' href="<?php echo $paintingsFile[$m]["imgURL"]; ?>">Learn More</a>
                        </p>
                    </div>
                </div>
        <?php
                }
        ?>
                </div>
                
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>

            
        </div>
        
        <div class="container">
        <?php
        if (isset($paintingsFile))
            for ($n=5 ; $n<11 ; $n++){
                
                if ($n==5 || $n==8){
                    echo "<div class='row'>";
                }
        ?>
            <div class = "col-md-4 content">
                <img class="img-circle" src="art-images/paintings/medium/<?php echo $paintingsFile[$n]["paintingID"]; ?>.jpg" alt="<?php echo $paintingsFile[$n]["paintingTitle"]; ?>" title="<?php echo $paintingsFile[$n]["paintingTitle"]; ?>">
                <h3><?php echo $paintingsFile[$n]["paintingTitle"]; ?></h3>
        <?php
            #truncate description lines into the first line
             $s = strstr (  $paintingsFile[$n]["paintingDesc"], ".", 1);
        ?>
                <p id = "desc"><?php echo $s; ?></p>
                <p>
                    <a class="btn btn-lg btn-default outline" href="<?php echo $paintingsFile[$n]["imgURL"]; ?>">View Details »</a>
                </p>
            </div>
        <?php
             if ($n==7 || $n==10){
                    echo "</div>";
                }
        
            } 
        ?>
    </div>
    </body>
</html>