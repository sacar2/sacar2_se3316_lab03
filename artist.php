<?php
    # read the file into an array, else, output error
    $artistFile = file("data-files/artists.txt") or die("ERROR! Unable to open artists file!!!");

    #our file is tilde delimited
    $delimiter = "~";
    
    $i = 0;
    foreach ($artistFile as $artist){
        $artistFields = explode($delimiter, $artist);
        
        $artistFile[$i] = [
                            "artist_id"=>$artistFields[0],
                            "fname" => $artistFields[1], 
                            "lname" => $artistFields[2], 
                            "nationality" => $artistFields[3], 
                            "yob" => $artistFields[4], 
                            "yod" => $artistFields[5], 
                            "artistDesc" => $artistFields[6], 
                            "artistURL" => $artistFields[7]
                            ];
    $i++;
    }
?>
<!DOCTYPE html>
<html>
  <head>
        <title>Lab3 SE3316A</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- FONTS -->
        <link href='https://fonts.googleapis.com/css?family=Parisienne' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css'>

        <!-- LINKS & SCRIPTS -->
        <link rel="stylesheet" href="styles2.css">
            <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
            <!-- jQuery library -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <!-- Latest compiled JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
  <body>
    <header class="container">
        <!-- BANNER NAV -->
        <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header">
              <p>
                Welcome to <strong>Art Store</strong>, <a href="#">Login</a> or <a href="#">Create new Account</a>
              </p>
            </div>
            
            <div id ="upnav" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> My Account</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span> Wish List</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Shopping Cart</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Checkout</a></li>
              </ul>
            </div>
          </div>
        </nav>
              
        <!-- BANNER Brand/search -->
        <div class="row">
          <!-- Art store -->
          <div class="col-md-8">
            <h1 id="title">Art Store</h1>
          </div>
          <!-- Search Bar -->
          <div id = "search" class="input-group col-md-3">
            <form class="form-inline" role="search">
              <div class="input-group">
                <label class="sr-only" for="Search">Search</label>
                <input type="text" class="form-control" placeholder="Search" name="search">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                  </button>
                </span>
              </div>
            </form>
          </div>
        </div>
        
        <!-- NAV BAR -->
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header fontsi">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav fontsi">
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About Us</a></li>
                <li><a href="work.php">Art Works</a></li>
                <li class ="active"><a href="artist.php">Artists</a></li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Specials
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Special 1</a></li>
                    <li><a href="#">Special 2</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    
    <div class="container">
      <h2 class="fontsi">This Week's Best Artists</h2>
      <div class="alert alert-warning" role="alert">Each week we show you our best artists...</div>
      <div class="row">
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            
            <?php
            $m = 0;
            while ($m<18){
              if($m == 0)
                  {
                   echo "<div class='item active'>";
                    echo "<div class='container'>";
                  }
                  if( $m==6 || $m==12 )
                  {
                   echo "<div class='item'>";
                     echo "<div class='container'>";
                  }
              
            ?>
                      <div class='col-md-2 artist'>
                        <div class="thumbnail">
                          <img src='art-images/artists/small/<?php echo $artistFile[$m]["artist_id"]; ?>.jpg' alt ="<?php echo $artistFile[$m]["fname"].$artistFile[$m]["lname"]; ?>" >
                          <br>
                          <div class='caption'>
                            <p class="fontsi"><?php echo $artistFile[$m]["fname"]." ".$artistFile[$m]["lname"]; ?></p>
                            <p>
                              <a class='btn btn-info' role="button" href="<?php echo $artistFile[$m]["artistURL"]; ?>">Learn More</a>
                            </p>
                          </div><!--CAPTION-->
                        </div><!--thumbnail-->
                      </div><!--col-md-2-->
            
            
            <?php
              if($m == 5 || $m==11 || $m==17)
                {
                  echo "</div><!-- container-->";
                echo "</div><!-- item-->";
                }
            $m++;  
            }  
            ?>
            
          </div><!-- carousel-inner-->
          
          <a class="left li carousel-control" href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right lo carousel-control" href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div><!-- carousel slide -->
      </div><!--row--> 
      
      <div class="row">
        <div id="carousel2" class="carousel slide" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            
            <?php
            $m = 18;
            while ($m<36){
              if($m == 18)
                  {
                   echo "<div class='item active'>";
                    echo "<div class='container'>";
                  }
                  if( $m==24 || $m==30 )
                  {
                   echo "<div class='item'>";
                     echo "<div class='container'>";
                  }
              
            ?>
                      <div class='col-md-2 artist'>
                        <div class="thumbnail">
                          <img src='art-images/artists/small/<?php echo $artistFile[$m]["artist_id"]; ?>.jpg' alt ="<?php echo $artistFile[$m]["fname"].$artistFile[$m]["lname"]; ?>" >
                          <br>
                          <div class='caption'>
                            <p class="fontsi"><?php echo $artistFile[$m]["fname"]." ".$artistFile[$m]["lname"]; ?></p>
                            <p>
                              <a class='btn btn-info' role="button" href="<?php echo $artistFile[$m]["artistURL"]; ?>">Learn More</a>
                            </p>
                          </div><!--CAPTION-->
                        </div><!--thumbnail-->
                      </div><!--col-md-2-->
            
            
            <?php
              if($m == 23 || $m==29 || $m==35)
                {
                  echo "</div><!-- container-->";
                echo "</div><!-- item-->";
                }
            $m++;  
            }  
            ?>
            
          </div><!-- carousel-inner-->
          
          <a class="left li carousel-control" href="#carousel2" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right lo carousel-control" href="#carousel2" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div><!-- carousel slide -->
      </div><!--row--> 
      
      <h4>Artists By Genre</h4>
      <div class="progress">
        <div class="progress-bar progress-bar-info" style="width:7%"><span>Gothic</span></div>
        <div class="progress-bar progress-bar-success" style="width:27%"><span>Renaissance</span></div>
        <div class="progress-bar progress-bar-warning" style="width:15%"><span>Baroque</span></div>
        <div class="progress-bar progress-bar-danger" style="width:21%"><span>Pre-Modern</span></div>
        <div class="progress-bar" style="width:30%"><span>Modern</span></div>
      </div><!-- progress -->
      
      
    </div><!--container-->  
    
  </body>
</html>