        <?php
            # read the file into an array, else, output error
            $paintingsFile = file("data-files/paintings.txt") or die("ERROR! Unable to open paintings file!!!");

            #our file is tilde delimited
            $delimiter = "~";
            
            $j = 0;
            foreach ($paintingsFile as $paintings){
                $paintingsFields = explode($delimiter, $paintings);
                $paintingsFile[$j] = [
                                    "rndmNo1"=>$paintingsFields[0],
                                    "rndmNo2" => $paintingsFields[1], 
                                    "rndmNo3" => $paintingsFields[2], 
                                    "paintingID" => $paintingsFields[3], 
                                    "paintingTitle" => $paintingsFields[4], 
                                    "paintingDesc" => $paintingsFields[5], 
                                    "paintingYear" => $paintingsFields[6], 
                                    "width" => $paintingsFields[7],
                                    "height" => $paintingsFields[8],
                                    "genre" => $paintingsFields[9],
                                    "gallery" => $paintingsFields[10],
                                    "price" => $paintingsFields[11],
                                    "imgURL" => $paintingsFields[12]
                                    ];
            $j++;
            }
        ?>
<!DOCTYPE html>
<html>
  <head>
      <title>Lab3 SE3316A</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <!-- FONTS -->
      <link href='https://fonts.googleapis.com/css?family=Parisienne' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css'>
      
      <!-- LINKS & SCRIPTS -->
      <link rel="stylesheet" href="styles2.css">
          <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
          <!-- jQuery library -->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
          <!-- Latest compiled JavaScript -->
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </head>
  <body>
    <!-- HEADER -->
    <header class="container">
      <!-- BANNER NAV -->
      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <p>
              Welcome to <strong>Art Store</strong>, <a href="#">Login</a> or <a href="#">Create new Account</a>
            </p>
          </div>
          <div id ="upnav" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> My Account</a></li>
              <li><a href="#"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span> Wish List</a></li>
              <li><a href="#"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Shopping Cart</a></li>
              <li><a href="#"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Checkout</a></li>
            </ul>
          </div>
        </div>
      </nav>
            
      <!-- BANNER Brand/search -->
      <div class="row">
        <!-- Art store -->
        <div class="col-md-8">
          <h1 id="title">Art Store</h1>
        </div>
        <!-- Search Bar -->
        <div id = "search" class="input-group col-md-3">
          <form class="form-inline" role="search">
            <div class="input-group">
              <label class="sr-only" for="Search">Search</label>
              <input type="text" class="form-control" placeholder="Search" name="search">
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                  <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </button>
              </span>
            </div>
          </form>
        </div>
      </div>
      
      <!-- NAV BAR -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul id="wrknav" class="nav navbar-nav fontsi">
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About Us</a></li>
              <li class ="active"><a href="work.php">Art Works</a></li>
              <li><a href="artist.php">Artists</a></li>
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Specials
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Special 1</a></li>
                  <li><a href="#">Special 2</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
      
    <!-- Carousel-->
    <!-- data-ride says start carousel immediately, class= carousel says this is a carousel, and class=slide indicates transition-->
    <div class="container">
      <div class="row">
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
      <?php
              $m = 0;
              foreach ($paintingsFile as $painting){
                  
              if($m == 0)
              {
                echo "<div class='item active'>";
              }
              else {
                echo "<div class='item'>";
              }
      ?>          <div class="container">
                    <div class="col-md-10">
                      <h2 class="fontsi"><?php echo $painting["paintingTitle"]; ?></h2>
                      <p><a href="#"><?php echo $painting['paintingYear']; ?></a></p>
                      <div class="row">
                        <div class="col-md-5">
                          <img class="img-thumbnail img-responsive" src='art-images/paintings/large/<?php echo $painting["paintingID"]; ?>.jpg' alt ="<?php echo $painting["paintingTitle"]; ?>"  title ="<?php echo $painting["paintingTitle"]; ?>">
                        </div>
                        <div class='col-md-7'>
                          <p style="text-align: justify; 	text-justify: inter-word;">for some reason, adding the product description in php in THIS area, causes <em><strong>ALL</strong></em> the paintings below the carousel to appear!! but if I DON'T enter the artwork description in php in this area (LIKE I DID IN THIS VERSION OF THE CODE - you can uncomment the line above this and try it out...) then everything is fine and only the painting that should be here appears and everything is perfect... it makes no sense to me!</p>
                          <!-- <p style="text-align: justify; 	text-justify: inter-word;"> <?php echo $painting['paintingDesc']; ?> </p>-->
                          <h4 class="price"><?php echo $painting['price']; ?></h4>
                          <div class="btn-group btn-group-lg">
                            <button class="btn btn-default" type="button">
                              <a href="#">
                                <span class="glyphicon glyphicon-gift"></span>
                                Add to Wish List
                              </a>
                            </button>
                            <button class="btn btn-default" type="button">
                              <a href="#">
                                <span class="glyphicon glyphicon-shopping-cart"></span>
                                Add to Shopping Cart
                              </a>
                            </button>
                          </div>
                          <p><br></p>
                          <div class="panel panel-default">
                            <div class="panel-heading">Product Details</div>
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th>Date:</th>
                                  <td><?php echo $painting['paintingYear']; ?></td>
                                </tr>
                                <tr>
                                  <th>Medium:</th>
                                  <td><?php echo $painting['genre']; ?></td>
                                </tr>
                                <tr>
                                  <th>Dimensions:</th>
                                  <td><?php echo $painting['height']?>cm x <?php echo $painting['width']; ?>cm</td>
                                </tr>
                                <tr>
                                  <th>Home:</th>
                                  <td><a href="#"><?php echo $painting['gallery']; ?></a></td>
                                </tr>
                                <tr>
                                  <th>Link:</th>
                                  <td><a href="<?php echo $painting['imgURL']; ?>">Wiki</a></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
      <?php
              $m++;
              }
      ?>
           </div>
          <!-- Left and right controls -->
          <a class="left li carousel-control arts" href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right lo carousel-control arts" href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </body>
</html>