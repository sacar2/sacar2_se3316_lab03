<?php
            # red the file into an array, else, output error
            $artistFile = file("data-files/artists.txt") or die("ERROR! Unable to open artists file!!!");
            $paintingsFile = file("data-files/paintings.txt") or die("ERROR! Unable to open paintings file!!!");

            #our file is tilde delimited
            $delimiter = "~";
            $j = 0;
            foreach ($paintingsFile as $paintings){
                $paintingsFields = explode($delimiter, $paintings);
                $artistFile[$j] = [
                                    "rndmNo1"=>$paintingsFields[0],
                                    "rndmNo2" => $paintingsFields[1], 
                                    "rndmNo3" => $paintingsFields[2], 
                                    "paintingID" => $paintingsFields[3], 
                                    "paintingTitle" => $paintingsFields[4], 
                                    "paintingDesc" => $paintingsFields[5], 
                                    "paintingYear" => $paintingsFields[6], 
                                    "width" => $paintingsFields[7],
                                    "height" => $paintingsFields[8],
                                    "genre" => $paintingsFields[9],
                                    "gallery" => $paintingsFields[10],
                                    "price" => $paintingsFields[11],
                                    "imgURL" => $paintingsFields[12]
                                    ];
            $j++;
            }
        ?>

<!DOCTYPE html>
<html>
    <head>
        <title>Lab3 SE3316A</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- FONTS -->
        <link href='https://fonts.googleapis.com/css?family=Parisienne' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css'>
        
        <!-- LINKS & SCRIPTS -->
        <link rel="stylesheet" href="styles2.css">
            <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
            <!-- jQuery library -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <!-- Latest compiled JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
      <div class="container">
      <!-- BANNER NAV -->
      <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header">
              <p>
                Welcome to <strong>Art Store</strong>, <a href="#">Login</a> or <a href="#">Create new Account</a>
              </p>
            </div>
            
            <div id ="upnav" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> My Account</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span> Wish List</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Shopping Cart</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Checkout</a></li>
              </ul>
            </div>
          </div>
        </nav>
            
      
      <div class="row">
        <!-- Art store -->
        <div class="col-md-8">
          <h1 id="title">Art Store</h1>
        </div>
        <!-- Search Bar -->
        <div id = "search" class="input-group col-md-3">
          <form class="form-inline" role="search">
            <div class="input-group">
              <label class="sr-only" for="Search">Search</label>
              <input type="text" class="form-control" placeholder="Search" name="search">
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                  <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </button>
              </span>
            </div>
          </form>
        </div>
      </div>
      
        <!-- NAV BAR -->
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav fontsi">
                <li><a href="index.php">Home</a></li>
                <li class ="active"><a href="about.php">About Us</a></li>
                <li><a href="work.php">Art Works</a></li>
                <li><a href="artist.php">Artists</a></li>

                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Specials
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Special 1</a></li>
                    <li><a href="#">Special 2</a></li>
                  </ul>
                </li>
                
              </ul>
            </div>
          </div>
        </nav>
        
      <div id="about" class="jumbotron">
        <div class="container">
          <h2 class="fontsi">About Us</h2>
          <p>
            This assignment submission was created by: 
            <br>
            <br>
            <em>Selin Denise Acar {sacar2@uwo.ca}</em>
            <br>
            <br>
            A student at Western University taking SE3316A.
          </p>
          <a class="btn btn-lg btn-primary" href="http://owl.uwo.ca" role="button">
            Learn More
          </a>
        </div>
      </div>
    
    </div>
    </body>
</html>